import Foundation
import CreateML

let workPath = "/Users/georgeostrobrod/uni/CoreML Tutor/2. Training - Playground"
let maxIter = 1000

// Load source data
let dataSource = MLImageClassifier.DataSource.labeledDirectories(at: URL(fileURLWithPath: "/Users/georgeostrobrod/uni/CoreML Tutor/0. DataSet/ArTaxOr"))
let splitData = try! dataSource.stratifiedSplit(proportions: [0.8, 0.2])
let trainData = splitData[0]
let testData = splitData[1]



// Create classifier
let augmentation = MLImageClassifier.ImageAugmentationOptions(arrayLiteral: [
//	MLImageClassifier.ImageAugmentationOptions.blur,
//	MLImageClassifier.ImageAugmentationOptions.exposure,
//	MLImageClassifier.ImageAugmentationOptions.flip,
//	MLImageClassifier.ImageAugmentationOptions.noise
])
let trainParams = MLImageClassifier.ModelParameters(validation: MLImageClassifier.ModelParameters.ValidationData.split(strategy: .automatic),
													maxIterations: maxIter,
													augmentation: augmentation)

let classifier = try! MLImageClassifier(trainingData: trainData, parameters: trainParams)

/// Classifier training accuracy as a percentage
let trainingError = classifier.trainingMetrics.classificationError
let trainingAccuracy = (1.0 - trainingError) * 100

let validationError = classifier.validationMetrics.classificationError
let validationAccuracy = (1.0 - validationError) * 100

/// Evaluate the classifier
let classifierEvaluation = classifier.evaluation(on: testData)
let evaluationError = classifierEvaluation.classificationError
let evaluationAccuracy = (1.0 - evaluationError) * 100

// Save model
let homePath = URL(fileURLWithPath: workPath)
let classifierMetadata = MLModelMetadata(author: "George Ostrobrod",
										 shortDescription: "Predicts order of insect.",
										 version: "1.0")
try classifier.write(to: homePath.appendingPathComponent("InsectOrder.mlmodel"),
					 metadata: classifierMetadata)
