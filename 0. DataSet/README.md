1. Download [dataset]((https://www.kaggle.com/mistag/arthropod-taxonomy-orders-object-detection-dataset) and extract it here.
2. Run `convert_dataset.py`.
3. Move `Annotations.json` to `./ArTaxOr`.