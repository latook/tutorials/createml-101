import os
import json

annotations = []
src_folders = [
    'Araneae',
    'Coleoptera',
    'Diptera',
    'Hemiptera',
    'Hymenoptera',
    'Lepidoptera',
    'Odonata',
]

for folder in src_folders:
    cur_dir = 'ArTaxOr/' + folder + '/annotations'
    file_names = os.listdir(cur_dir)
    for file_name in file_names:
        if file_name[0] == '.':
            continue
        if file_name[-4:] != 'json':
            continue
        print(cur_dir + '/' + file_name)
        with open(cur_dir + '/' + file_name) as f:
            src = json.load(f)
            image_dict = {"image": '', "annotations": []}
            image_dict['image'] = folder + '/' + src['asset']['name']

            for region in src['regions']:
                label_dict = {"label": '', "coordinates": {}}
                coord_dict = {"x": int, "y": int, "width": int, "height": int}
                w = region['boundingBox']['width']
                h = region['boundingBox']['height']
                coord_dict['x'] = region['boundingBox']['left'] + w / 2
                coord_dict['y'] = region['boundingBox']['top'] + h / 2
                coord_dict['width'] = w
                coord_dict['height'] = h

                label_dict['label'] = region['tags'][0]
                label_dict['coordinates'] = coord_dict

                image_dict['annotations'].append(label_dict)

            annotations.append(image_dict)


print('Number of Processed Images:', len(annotations))

json_file = json.dumps(annotations)
with open('Annotations.json', 'w') as f:
    f.write(json_file)
