//
//  MacInsectsClassifierApp.swift
//  MacInsectsClassifier
//
//  Created by Adam Croser on 12/5/21.
//

import SwiftUI

@main
struct MacInsectsClassifierApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
