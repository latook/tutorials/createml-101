//
//  ContentView.swift
//  MacInsectsClassifier
//
//  Created by Adam Croser on 12/5/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MainScreen()
			.frame(minWidth: 1024, idealWidth: 1024, maxWidth: .infinity, minHeight: 768, idealHeight: 768, maxHeight: .infinity)
			.presentedWindowStyle(HiddenTitleBarWindowStyle())
			.presentedWindowToolbarStyle(UnifiedCompactWindowToolbarStyle())
			.onDisappear() {
				exit(0)
			}

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
